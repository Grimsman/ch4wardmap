from Tkinter import *

class MoveOutWindow:

    def __init__(self, root, name, addr):
        
        self.tl = Toplevel(root)
        self.tl.wm_title("Move Household Out")
        x = root.winfo_rootx()
        y = root.winfo_rooty()
        geom = "+%d+%d" % (x,y)
        self.tl.geometry(geom)
        img = PhotoImage(file='ldsicon.gif')
        self.tl.tk.call('wm', 'iconphoto', self.tl._w, img)
        Label(self.tl, text="The {} household is not found at {} in the imported list"\
              " in the\ndirectory. What would you like to do?" \
              .format(name, addr), justify=LEFT).grid(row=0, column=0)
        self.v = IntVar()
        Radiobutton(self.tl, text="Ignore this and leave the household on the map"\
                    , justify=LEFT, variable=self.v, value=1).grid(row=1, column=0, sticky=W)
        Radiobutton(self.tl, text="Remove the household from the ward", justify=LEFT\
                    , variable=self.v, value=2).grid(row=2, column=0, sticky=W)
        Radiobutton(self.tl, text='Move this household out of the ward but keep them on the map. Use this option if the household has not moved, but moved records to a new ward.' \
                    , justify=LEFT, variable=self.v, value=3).grid(row=3, column=0, sticky=W)
        Button(self.tl, text="OK", command=self.closeWin).grid(row=4, column=0)

    def closeWin(self):
        self.tl.destroy()

    def get_val(self):
        return self.v.get()
