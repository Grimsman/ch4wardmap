from Tkinter import *

class AddressNotFoundWindow:
    
    def __init__(self, root, name, addr):
        self.v = 2
        self.tl = Toplevel(root)
        self.v = IntVar()
        self.tl.wm_title("Address Not Found")
        x = root.winfo_rootx()
        y = root.winfo_rooty()
        geom = "+%d+%d" % (x,y)
        self.tl.geometry(geom)
        img = PhotoImage(file='ldsicon.gif')
        self.tl.tk.call('wm', 'iconphoto', self.tl._w, img)
        Label(self.tl, text="The address {} for the {} household is not\nfound in the ward list. " \
              "What would you like to do?".format(addr, name), justify=LEFT).grid(row=0, column=0)
        Radiobutton(self.tl, text="Add this address to the map list.", variable=self.v, value=1, justify=LEFT)\
                       .grid(row=1, column=0, sticky=W)
        Label(self.tl, text="Note: You will need to manually add the house to the map\nand " \
              "use the VLOOKUP function to link to the address. See\nthe other houses on the map " \
              "for examples.", justify=LEFT).grid(row=2, column=0, sticky=W)
        Radiobutton(self.tl, text="Add this family to the unknown address list", variable=self.v, value=2, justify=LEFT) \
                    .grid(row=3, column=0, sticky=W)
        Radiobutton(self.tl, text="Ignore this household - do not put it on the ward map.", variable=self.v, value=3, justify=LEFT)\
                    .grid(row=4, column=0, sticky=W)
        Button(self.tl, text="OK", command=self.closeWin).grid(row=5, column=0)

    def closeWin(self):
        self.val = self.v.get()
        self.tl.destroy()

    def get_val(self):
        return self.val

