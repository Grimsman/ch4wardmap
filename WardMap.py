from xlwings import *
import pandas as pd
from AddressNotFoundWindow import AddressNotFoundWindow
from MoveInWindow import MoveInWindow
from MoveOutWindow import MoveOutWindow
from ColumnsNotSetWindow import ColumnsNotSetWindow
from CorrectedAddressWindow import CorrectedAddressWindow
from Tkinter import Tk, Toplevel
import string

UNKNOWN_STR = '???'

class WardMap:

    def __init__(self, wb_path, csv_path, root):
        self.root = root
        self.wb = Workbook(wb_path)
        print wb_path
        self.ml = Range(Sheet('Map List'), 'MapListTbl').options(pd.DataFrame, index=False, header=False).value
        self.ml.columns = ['Address', 'Name', 'LDS', 'WardMember', 'AbbrevName', 'Cell']
        self.il = pd.read_csv(csv_path)
        self.il = self.il[['Family Name', 'Family Address']]
        self.il.columns = ['Name', 'Address']
        # format all addresses
        for i in range(len(self.il.index)):
            self.il.iloc[i,1] = self.format_addr(str(self.il.iloc[i,1]))
    
    def format_addr(self, addr):
    
        addr = addr.translate(string.maketrans("",""), string.punctuation)
        l = addr.split()
        
        # see whether it's in orem, if so, cut off city/state/zip info
        in_orem = False
        for i, word in enumerate(l):
            if word.lower() == "orem":
                l = l[:i]
                in_orem = True
                break
        
        # if it's not in orem, return original address
        if not in_orem:
            return addr

        # make sure that certain abbreviations are used
        for i, word in enumerate(l):
            if word.lower() == "south" or word == "s":
                l[i] = "S"
            elif word.lower() == "east" or word == "e":
                l[i] = "E"
            elif word.lower() == "state":
                l[i] = "State"
            elif word.lower() == "street" or word.lower() == "st":
                l[i] = "St"
            elif word.lower() == "columbia":
                l[i] = "Columbia"
            elif word.lower() == "lane" or word.lower() == "ln":
                l[i] = "Ln"
            elif word.lower() == "apartment" or word.lower() == "apt":
                l.pop(i)
            elif word.lower() == "basement" or word.lower() == "bsmt":
                l.pop(i)
            elif word.lower() == "unit" or word.lower() == "unt":
				l.pop(i)
        
        # make sure "St" follows "State"
        for i, word in enumerate(l):
            if word == "State":
                if i == len(l) - 1:
                    l.append("St")
                elif l[i+1] != "St":
                    l.insert(i+1, "St")
                break

        # make sure that the apartment number is formatted correctly
        if l[-1].isdigit() or l[-1][1:].isdigit() or l[-1].lower() == 'a':
            apt_num = l[-1]
            if not apt_num[0].isdigit():
                apt_num = apt_num[0].upper() + apt_num[1:]
            l[-1] = '#' + apt_num
        
        # turn the list back into a str
        s = ''
        for word in l:
            s = s + word + ' '
        return s[:-1]
    
    def sync(self):
    
        # check the map list for each address
        for il_row in self.il.iterrows():
            il_name = il_row[1]['Name']
            il_addr = il_row[1]['Address']
            if il_addr in self.ml['Address'].values:
                ml_row = self.ml.loc[self.ml['Address'] == il_addr]
                ml_names = ml_row['Name'].values[0].split('/')
                if il_name not in ml_names:
                    if self.is_unknown(il_name):
                        opt = self.correctedaddress(il_name, il_addr)
                        if opt == 1:
                            self.remove_name_from_unknown(il_name)
                    print 'new move in dialog: {} at {}'.format(il_name, il_addr)
                    ml_names = self.movein(il_name, il_addr, ml_names)
                    names_str = ''
                    for name in ml_names:
                        names_str += name + '/'
                    names_str = names_str[:-1]
                    self.ml.Name[self.ml.Address==il_addr] = names_str
                    self.ml.LDS[self.ml.Address==il_addr] = 'Yes'
                    self.ml.WardMember[self.ml.Address==il_addr] = 'Yes'
            elif not self.is_unknown(il_name):
                print 'launch new address in ward dialog for {} at {}'.format(il_name, il_addr)
                opt = self.addressnotfound(il_name, il_addr)
                if opt == 1:
                    new_row = pd.DataFrame({'Address':il_addr, 'Name':il_name, 'LDS':'Yes', 'WardMember':'Yes'}, index=[0])
                    self.ml = self.ml.append(new_row)
                elif opt == 2:
                    self.add_name_to_unknown(il_name)

        # check each address in the map list to see whether it matches the imported list
        for ml_row in self.ml.iterrows():
            if ml_row[1]['WardMember'] == 'No' or ml_row[1]['AbbrevName'] == UNKNOWN_STR:
                continue
            ml_names = ml_row[1]['Name'].split('/')
            ml_addr = ml_row[1]['Address']
            launch_moveout = False
            if ml_addr in self.il['Address'].values:
                il_row = self.il.loc[self.il['Address'] == ml_addr]
                il_names = il_row['Name'].values
                for name in ml_names:
                    if name not in il_names:
                        print 'launch move out dialog for {} at {}'.format(name, ml_addr)
                        rem_name = self.moveout(name, ml_addr)
                        if rem_name:
                            ml_names.remove(name)
                            if not ml_names:
                                ml_row[1]['Name'] = UNKNOWN_STR
                                ml_row[1]['AbbrevName'] = UNKNOWN_STR
                                ml_row[1]['LDS'] = ''
                                ml_row[1]['WardMember'] = ''
                            else:
                                ml_row[1]['Name'] = '/'.join(ml_names)
            elif ml_addr != UNKNOWN_STR:
                for name in ml_names:
                    print 'launch move out dialog for {} at {}'.format(name, ml_addr)
                    opt = self.moveout(name, ml_addr)
                    if opt == 2:
                        ml_names.remove(name)
                        if not ml_names:
                            ml_row[1]['Name'] = UNKNOWN_STR
                            ml_row[1]['AbbrevName'] = UNKNOWN_STR
                            ml_row[1]['LDS'] = ''
                            ml_row[1]['WardMember'] = ''
                        else:
                            ml_row[1]['Name'] = '/'.join(ml_names)
                    elif opt == 3:
                        ml_row[1]['WardMember'] = 'No'

        for ml_row in self.ml.iterrows():
            if ml_row[1]['Name'] == '':
                ml_row[1] = UNKNOWN_STR
            if ((ml_row[1]['WardMember'] != 'Yes' and ml_row[1]['WardMember'] != 'No') or\
               (ml_row[1]['LDS'] != 'Yes' and ml_row[1]['LDS'] != 'No')) and\
               ml_row[1]['Name'] != UNKNOWN_STR and ml_row[1]['Address'] != UNKNOWN_STR:
                opt = self.check_columns(ml_row[1]['Name'])
                if opt == 1:
                    ml_row[1]['LDS'] = 'No'
                    ml_row[1]['WardMember'] = 'No'
                elif opt == 2:
                    ml_row[1]['LDS'] = 'Yes'
                    ml_row[1]['WardMember'] = 'No'
                else:
                    ml_row[1]['LDS'] = 'Yes'
                    ml_row[1]['WardMember'] = 'Yes'

        self.abbrev_all_names()
        self.format_cells()
        self.write_names_in_cells()
            
        self.ml = self.ml[["Address", "Name", "LDS", "WardMember", "AbbrevName", "Cell"]]
        Range(Sheet('Map List'), 'MapListTbl').options(pd.DataFrame, index=False, header=False).value = self.ml
    
    def check_columns(self, name):
        win = ColumnsNotSetWindow(self.root, name)
        self.root.wait_window(win.tl)
        return win.get_val()
               
    def movein(self, name, addr, curr_res):
        win = MoveInWindow(self.root, name, addr, curr_res)
        self.root.wait_window(win.tl)
        val = win.get_val()
        if val == 2:
            if curr_res[0] == UNKNOWN_STR:
                curr_res[0] = name
            else:
                curr_res.append(name)
        elif val == 3:
            moveouts = win.get_moveouts()
            curr_res.append(name)
            for moveout in moveouts:
                curr_res.remove(moveout)

        return curr_res

    def addressnotfound(self, name, addr):
        win = AddressNotFoundWindow(self.root, name, addr)
        self.root.wait_window(win.tl)
        return win.get_val()

    def moveout(self, name, addr):
        win = MoveOutWindow(self.root, name, addr)
        self.root.wait_window(win.tl)
        return win.get_val()
    
    def abbrev_name(self, name, cell_str):
        if cell_str is None:
            return
        sht_name = cell_str.split('/')[0]
        sht = Sheet(sht_name)
        cell_name = cell_str.split('/')[1]
        cell = Range(sht, cell_name)
        w = cell.column_width
        font_sz = cell.xl_range.Font.Size
        
        sht = Sheet('TestWidth')
        abbrev_name = name
        cell = Range(sht, 'A1')
        cell.xl_range.Font.Size = font_sz
        cell.value = abbrev_name
        cell.autofit('c')
        abbrev_w = cell.column_width
        
        while abbrev_w > w:
            abbrev_name = self.min_all_names(abbrev_name)
            cell.value = abbrev_name
            cell.autofit('c')
            abbrev_w = cell.column_width
        
        return abbrev_name
    
    def min_all_names(self, names):
        max_name = ''
        max_i = -1
        names_list = names.split('/')
        for i, name in enumerate(names_list):
            if len(max_name) < len(name):
                max_name = name
                max_i = i
        names_list[max_i] = names_list[max_i][:-1]
        names = '/'.join(names_list)
        return names

    def format_cells(self):
        sht = Sheet('Map List')
        for row in self.ml.iterrows():
            name = row[1]['Name']
            if row[1]['Cell'] is None:
                continue
            print row[1]['Cell']
            cell_strs = row[1]['Cell'].split('/')
            cell = Range(cell_strs[0], cell_strs[1])
            if row[1]['LDS'] == 'No':
                cell.xl_range.Font.Bold = True
            elif row[1]['WardMember'] == 'No':
                cell.xl_range.Font.Italic = True
            else:
                cell.xl_range.Font.Bold = False
                cell.xl_range.Font.Italic = False
    
    def write_names_in_cells(self):
        # clear values for unlisted households
        str_l = self.ml['Cell'][self.ml['Address'] == UNKNOWN_STR].values[0].split('/')
        nl_sht = Sheet(str_l[0])
        nl_cell_name = str_l[1]
        not_listed_cell = Range(nl_sht, nl_cell_name)
        cell_name = nl_cell_name
        ws_col = cell_name[0]
        ws_row = int(cell_name[1:])
        while Range(nl_sht, cell_name).value is not None:
            Range(nl_sht, cell_name).value = None
            ws_row += 1
            cell_name = ws_col + str(ws_row)
        
        for row in self.ml.iterrows():
            name = row[1]['AbbrevName']
            if name is None:
                name = UNKNOWN_STR
             
            if row[1]['Address'] == UNKNOWN_STR:
                if name == UNKNOWN_STR:
                    not_listed_cell.value = '[None]'
                cell_name = nl_cell_name
                ws_col = cell_name[0]
                ws_row = int(cell_name[1:])
                names = row[1]['Name'].split('/')
                for name in names:
                    Range(nl_sht, cell_name).value = name
                    ws_row += 1
                    cell_name = ws_col + str(ws_row)
            else:    
                cell_strs = row[1]['Cell'].split('/')
                cell = Range(cell_strs[0], cell_strs[1])
                cell.value = name
                
    def abbrev_all_names(self):
        for row in self.ml.iterrows():
            if row[1]['Address'] == UNKNOWN_STR:
                continue
            name = row[1]['Name']
            cell = row[1]['Cell']
            if cell is None:
                cell = NOT_LISTED_CELL
            abbrev = self.abbrev_name(name, cell)
            row[1]['AbbrevName'] = abbrev
            
        Range(Sheet('Map List'), 'MapListTbl').options(pd.DataFrame, index=False, header=False).value = self.ml

    def is_unknown(self, name):
        u_names = self.ml['Name'][self.ml['Address'] == UNKNOWN_STR].values[0]
        return u_names.find(name) != -1
        
    def add_name_to_unknown(self, name):
        unknown_names = self.ml['Name'][self.ml['Address'] == UNKNOWN_STR].values[0]
        unknown_names_list = unknown_names.split('/')
        unknown_names_list.append(name)
        unknown_names = '/'.join(unknown_names_list)
        self.ml['Name'][self.ml['Address'] == UNKNOWN_STR] = unknown_names
        
    def remove_name_from_unknown(self, name):
        unknown_names = self.ml['Name'][self.ml['Address'] == UNKNOWN_STR].values[0]
        unknown_names_list = unknown_names.split('/')
        unknown_names_list.remove(name)
        unknown_names = '/'.join(unknown_names_list)
        self.ml['Name'][self.ml['Address'] == UNKNOWN_STR] = unknown_names
        
    def correctedaddress(self, name, addr):
        win = CorrectedAddressWindow(self.root, name, addr)
        self.root.wait_window(win.tl)
        return win.get_val()














 
 
 
