from distutils.core import setup
import py2exe
import zmq.libzmq
import numpy
import matplotlib

setup(
    data_files=matplotlib.get_py2exe_datafiles(),
    options={
        'py2exe': {
            'includes': ['zmq.backend.cython'],
            'excludes': ['zmq.libzmq'],
            'dll_excludes': ['libzmq.pyd', 'MSVCP90.dll', 'numpy-atlas.dll'],
        }
    },
    windows = ['run_me.py'],
    zipfile = None
)