from Tkinter import *

UNKNOWN_STR = '???'

class MoveInWindow:

    def __init__(self, root, name, addr, curr_res):
        self.tl = Toplevel(root)
        self.curr_res = curr_res
        self.tl.wm_title("Move Household In")
        x = root.winfo_rootx()
        y = root.winfo_rooty()
        geom = "+%d+%d" % (x,y)
        self.tl.geometry(geom)
        img = PhotoImage(file='ldsicon.gif')
        self.tl.tk.call('wm', 'iconphoto', self.tl._w, img)
        self.rbv = IntVar()
        if self.curr_res[0] == UNKNOWN_STR:
            Label(self.tl, text="The address {} associated with the {} household is currently\nvacant."\
              " How would you like to handle this?".format(addr, name), justify=LEFT).grid(row=0, column=0)
            Radiobutton(self.tl, text="Ignore the {} household and do not put in on the map".format(name), justify=LEFT, \
                    variable=self.rbv, value=1).grid(row=1, column=0, sticky=W)
            Radiobutton(self.tl, text="Move the {} household into this residence.".format(name), \
                    justify=LEFT, variable=self.rbv, value=2).grid(row=2, column=0, sticky=W)
            rw = 4
        else:
            Label(self.tl, text="The address {} associated with the {} household is currently\noccupied by another household."\
                  " How would you like to handle this?".format(addr, name), justify=LEFT).grid(row=0, column=0)
            Radiobutton(self.tl, text="Ignore the {} household and do not put in on the map".format(name), justify=LEFT, \
                    variable=self.rbv, value=1, command=self.disableCbs).grid(row=1, column=0, sticky=W) 
            Radiobutton(self.tl, text="Add the {} household as an additional household at the residence.".format(name), \
                        justify=LEFT, variable=self.rbv, value=2, command=self.disableCbs).grid(row=2, column=0, sticky=W)
            Radiobutton(self.tl, text="Move the {} household in and move the following househols(s) out (check all that" \
                        " apply):".format(name), justify=LEFT, variable=self.rbv, value=3, command=self.enableCbs)\
                        .grid(row=3, column=0, sticky=W)
            self.cbs = []
            rw = 4
            for res in self.curr_res:
                cbv = IntVar()
                c = Checkbutton(self.tl, text=res, variable=cbv, state=DISABLED)
                c.grid(row=rw, column=0)
                c.var = cbv
                self.cbs.append(c)
                rw += 1
        
        Button(self.tl, text="OK", command=self.closeWin).grid(row=rw, column=0)
            
    def closeWin(self):
        self.tl.destroy()

    def enableCbs(self):
        for cb in self.cbs:
            cb['state'] = 'normal'
            
    def disableCbs(self):
        for cb in self.cbs:
            cb['state'] = 'disabled'

    def get_val(self):
        return self.rbv.get()

    def get_moveouts(self):
        res = []
        for i, cb in enumerate(self.cbs):
            if cb.var.get() == 1:
                res.append(self.curr_res[i])
        return res
        
