from Tkinter import *
from tkFileDialog import askopenfilename
from WardMap import WardMap

PADDING = 20

class StartWindow:       
    def __init__(self, master):
        self.master = master
        self.master.wm_title("Sync Ward Map")
        self.master.columnconfigure(0, pad=PADDING)
        self.master.columnconfigure(1, pad=PADDING)
        self.master.columnconfigure(2, pad=PADDING)
        self.master.columnconfigure(3, pad=PADDING)
        self.master.rowconfigure(1, pad=PADDING)
        self.master.rowconfigure(2, pad=10)
        self.master.rowconfigure(3, pad=PADDING)
        #self.master.iconbitmap('c:\\users\\david\\pycharmprojects\\ch4wardmap\\lds.ico')
        img = PhotoImage(file='ldsicon.gif')
        self.master.tk.call('wm', 'iconphoto', self.master._w, img)
        self.master.minsize(600, 160)
        self.filename_csv="C:\\Users\\David\\Downloads\\1979671.csv"
        self.filename_xlsx="C:\\Users\\David\\Downloads\\Cherry Hill 4th Ward Map.xlsm"
        csvfile=Label(master, text="Downloaded CSV File").grid(row=1, column=0)
        xlsxfile=Label(master, text="Ward Map Excel File").grid(row=2, column=0)
        entry_width = 60
        self.bar_csv=Entry(master, width=entry_width)
        self.bar_csv.grid(row=1, column=1) 
        self.bar_xlsx=Entry(master, width=entry_width)
        self.bar_xlsx.grid(row=2, column=1)

        #Buttons  
        self.cbutton= Button(master, text="Sync", command=self.begin_sync, width=50)
        self.cbutton.grid(row=3, column=1)
        self.bbutton_csv= Button(master, text="Browse", command=self.browsecsv)
        self.bbutton_csv.grid(row=1, column=3)
        self.bbutton_xlsx= Button(master, text="Browse", command=self.browsexlsx)
        self.bbutton_xlsx.grid(row=2, column=3)

    def browsecsv(self):
        Tk().withdraw() 
        self.filename_csv = askopenfilename().replace("/", "\\")
        self.bar_csv.delete(0, END)
        self.bar_csv.insert(0, self.filename_csv)
    
    def browsexlsx(self):
        Tk().withdraw() 
        self.filename_xlsx = askopenfilename().replace("/", "\\")
        self.bar_xlsx.delete(0, END)
        self.bar_xlsx.insert(0, self.filename_xlsx)

    def begin_sync(self):
        lbl_sync = Label(self.master, text="Performing Map Sync...")
        lbl_sync.grid(row=4, column=1)
        wmap = WardMap(self.filename_xlsx, self.filename_csv, self.master)
        self.bbutton_csv.config(state=DISABLED)
        self.bbutton_xlsx.config(state=DISABLED)
        self.cbutton.config(state=DISABLED)
        self.bar_csv.config(state=DISABLED)
        self.bar_xlsx.config(state=DISABLED)
        wmap.sync()
        lbl_sync.grid_remove()
        Label(self.master, text="Sync Complete!").grid(row=4, column=1)
        self.bbutton_csv.config(state=NORMAL)
        self.bbutton_xlsx.config(state=NORMAL)
        self.cbutton.config(state=NORMAL)
        self.bar_csv.config(state=NORMAL)
        self.bar_xlsx.config(state=NORMAL)