from Tkinter import *

class ColumnsNotSetWindow:
    
    def __init__(self, root, name):
        self.tl = Toplevel(root)
        self.v = IntVar()
        self.tl.wm_title("Household Membership")
        x = root.winfo_rootx()
        y = root.winfo_rooty()
        geom = "+%d+%d" % (x,y)
        self.tl.geometry(geom)
        img = PhotoImage(file='ldsicon.gif')
        self.tl.tk.call('wm', 'iconphoto', self.tl._w, img)
        Label(self.tl, text="The membership status of the {} household is not clear in the map.\n" \
              "Which of the following is true?".format(name), justify=LEFT).grid(row=0, column=0)
        Radiobutton(self.tl, text="This household contains no Church members.", variable=self.v, value=1, justify=LEFT)\
                       .grid(row=1, column=0, sticky=W)
        Radiobutton(self.tl, text="This household has members, but none of them are in the Cherry Hill 4th Ward.", variable=self.v, value=2, justify=LEFT) \
                    .grid(row=3, column=0, sticky=W)
        Radiobutton(self.tl, text="This household has Church members in the Cherry Hill 4th Ward", variable=self.v, value=3, justify=LEFT)\
                    .grid(row=4, column=0, sticky=W)
        Button(self.tl, text="OK", command=self.closeWin).grid(row=5, column=0)

    def closeWin(self):
        self.val = self.v.get()
        self.tl.destroy()

    def get_val(self):
        return self.val
