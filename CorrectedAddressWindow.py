from Tkinter import *

class CorrectedAddressWindow:
    
    def __init__(self, root, name, addr):
        self.tl = Toplevel(root)
        self.v = IntVar()
        self.tl.wm_title("Possible Corrected Address")
        x = root.winfo_rootx()
        y = root.winfo_rooty()
        geom = "+%d+%d" % (x,y)
        self.tl.geometry(geom)
        img = PhotoImage(file='ldsicon.gif')
        self.tl.tk.call('wm', 'iconphoto', self.tl._w, img)
        Label(self.tl, text="The {} household at {} matches a name of a household whose address is either previously unknown or not on the map.\n" \
              "Which of the following is true?".format(name, addr), justify=LEFT).grid(row=0, column=0)
        Radiobutton(self.tl, text="These are the same households.", variable=self.v, value=1, justify=LEFT)\
                       .grid(row=1, column=0, sticky=W)
        Radiobutton(self.tl, text="These are different households with the same name.", variable=self.v, value=2, justify=LEFT) \
                    .grid(row=3, column=0, sticky=W)
        # Radiobutton(self.tl, text="Ignore this household at this address.", variable=self.v, value=3, justify=LEFT)\
        #            .grid(row=4, column=0, sticky=W)
        Button(self.tl, text="OK", command=self.closeWin).grid(row=5, column=0)

    def closeWin(self):
        self.val = self.v.get()
        self.tl.destroy()

    def get_val(self):
        return self.val